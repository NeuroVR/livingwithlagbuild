# LivingWithLag

Experience VR with a additional latency of 500 ms!

Left Trigger: Toggle Delay  
Right Trigger: Fire  
Esc: Close App  


The project files can be found here: https://gitlab.com/NeuroVR/livingwithlag